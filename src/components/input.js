import React, {Component} from 'react'

export default class Input extends Component{

	render(){
		return(
			<div className="inputTag">

				<div className="form-group">
		          <label 
		            htmlFor={this.props.name} 
		            {...this.props.current === this.props.name && {style : {fontWeight:'bold'} }}
		            > {this.props.name} </label>

		          <input 
		            className="form-control"
		            name={this.props.name}
		            value={this.props.value}
		            onChange= {this.props.handleConversion}
		            placeholder={`Enter a ${this.props.thisName} value`}
		            />
		        </div>

			</div>

		);
	}
}
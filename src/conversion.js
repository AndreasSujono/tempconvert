export const celciusToFahrenheit = (celcius) => {
    return celcius*9/5 + 32
}

export  const celciusToKelvin = (celcius) => {
    return celcius+273
}

export  const fahrenheitToCelcius = (fahrenheit) => {
    return (fahrenheit-32)*5/9
}

export const fahrenheitToKelvin = (fahrenheit) => {
    return fahrenheitToCelcius(fahrenheit) + 273
}

export const kelvinToCelcius = (kelvin) => {
    return kelvin-273
}

export const kelvinToFahrenheit = (kelvin) => {
    return celciusToFahrenheit(kelvinToCelcius(kelvin))
}
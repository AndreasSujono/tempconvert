import React from 'react';
import logo from './logo.svg';
import './App.css';
import Input from './components/input'
import * as convert from './conversion'

class App extends React.Component {

  state = {
    current:'',
    celcius:'',
    fahrenheit:'',
    kelvin:'',
    error:false
  }

  handleConversion = (e) => {

    //check if input value is empty
    if (e.target.value === ''){
      console.log('null')
      this.setState(
        {
          celcius:'',
          fahrenheit:'',
          kelvin:'',
          error:false
        }
      )

      return  
    }
    if (e.target.value == '-'){
      this.setState(
        {
          current:e.target.name,
          celcius:'-',
          fahrenheit:'-',
          kelvin:'-',
          error:false
        }
      )
      return
    }

    const val = parseInt(e.target.value) 

    //check if the input value is a number
    if(Number.isNaN(val)) {
      console.log('Not a Number')
      this.setState({error:true})
      return 
    }


    

    if (e.target.name ==='celcius'){
      this.setState(
        {
          current:'celcius',
          celcius: val,
          fahrenheit: convert.celciusToFahrenheit(val),
          kelvin:convert.celciusToKelvin(val),
          error:false
        }
      )
    }
    

    else if (e.target.name ==='fahrenheit'){
      this.setState(
        {
          current:'fahrenheit',
          celcius: convert.fahrenheitToCelcius(val),
          fahrenheit: val,
          kelvin: convert.fahrenheitToKelvin(val),
          error:false
        }
      )
    }

    else if (e.target.name ==='kelvin'){
      this.setState(
        {
          current:'kelvin',
          celcius: convert.kelvinToCelcius(val),
          fahrenheit: convert.kelvinToFahrenheit(val),
          kelvin:val,
          error:false
        }
      )
    }

  }
  

  render(){

    return (
      <div className="App container">
        <h1> Temperature Converter </h1>
        <h5><b>Current Changed : </b>{this.state.current}</h5>

        {
          this.state.error &&
          <p className="text-danger"> Input a right number !! </p> 
        }

        <Input 
          current={this.state.current} 
          handleConversion={this.handleConversion}
          value={this.state.celcius}
          name="celcius"
        /> 

        <Input 
          current={this.state.current} 
          handleConversion={this.handleConversion}
          value={this.state.fahrenheit}
          name="fahrenheit"
        /> 

        <Input 
          current={this.state.current} 
          handleConversion={this.handleConversion}
          value={this.state.kelvin}
          name="kelvin"
        /> 



        <table className="table table-dark table-hover">
          <thead>
            <tr>
              <th> Celcius </th>
              <th> Fahrenheit </th>
              <th> Kelvin </th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td> {this.state.celcius} </td>
              <td> {this.state.fahrenheit} </td>
              <td> {this.state.kelvin} </td>
            </tr>
          </tbody>

        </table>

      </div>
    );

  }
  
}

export default App;
